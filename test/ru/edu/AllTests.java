package ru.edu;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.edu.model.AthleteImplTest;
import ru.edu.model.CountryParticipantImplTest;
import ru.edu.model.ParticipantImplTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AthleteImplTest.class,
        CountryParticipantImplTest.class,
        ParticipantImplTest.class,
        CompetitionImplTest.class
})
public class AllTests {
}
