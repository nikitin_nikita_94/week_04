package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AthleteImplTest {

    public static final Athlete FIRST_ATHLETE = new AthleteImpl("Николай", "Тюльков","Россия");
    public static final Athlete SECOND_ATHLETE = new AthleteImpl("Артур","Грикшас","Литва");
    public static final Athlete THREE_ATHLETE = new AthleteImpl("Никитин","Никита","Россия");
    public static final Athlete FORE_ATHLETE = new AthleteImpl("Лукашенко","Александр","Беларусь");
    public static final Athlete FIVE_ATHLETE = new AthleteImpl("Буш","Джо","Америка");
    public static final Athlete SIX_ATHLETE = new AthleteImpl("Маг","Ксардас","Миртана");

    public static List<Athlete> list;

    @Before
    public void setup() {
        list = new ArrayList<>();
        list.add(FIRST_ATHLETE);
        list.add(SECOND_ATHLETE);
        list.add(THREE_ATHLETE);
    }

    @Test
    public void getFirstName() {
        assertEquals(FIRST_ATHLETE.getFirstName(), list.get(0).getFirstName());
        assertEquals(SECOND_ATHLETE.getFirstName(), list.get(1).getFirstName());
        assertEquals(THREE_ATHLETE.getFirstName(), list.get(2).getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals(FIRST_ATHLETE.getLastName(),list.get(0).getLastName());
        assertEquals(SECOND_ATHLETE.getLastName(),list.get(1).getLastName());
        assertEquals(THREE_ATHLETE.getLastName(),list.get(2).getLastName());
    }

    @Test
    public void getCountry() {
        assertEquals(FIRST_ATHLETE.getCountry(),list.get(0).getCountry());
        assertEquals(SECOND_ATHLETE.getCountry(),list.get(1).getCountry());
        assertEquals(THREE_ATHLETE.getCountry(),list.get(2).getCountry());
    }

    @Test
    public void equalsTest() {
        assertEquals(FIRST_ATHLETE,list.get(0));
        assertNotEquals(FIRST_ATHLETE,list.get(1));
        assertNotEquals(FIRST_ATHLETE,null);
    }

    @Test
    public void hashCodeTest() {
        assertEquals(FIRST_ATHLETE.hashCode(),list.get(0).hashCode());
        assertEquals(SECOND_ATHLETE.hashCode(),list.get(1).hashCode());
        assertEquals(THREE_ATHLETE.hashCode(),list.get(2).hashCode());
    }

    @Test
    public void toStringAthleteTest() {
        assertEquals(FIRST_ATHLETE.toString(),list.get(0).toString());
        assertEquals(SECOND_ATHLETE.toString(),list.get(1).toString());
        assertEquals(THREE_ATHLETE.toString(),list.get(2).toString());
    }
}