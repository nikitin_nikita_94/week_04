package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static ru.edu.model.ParticipantImplTest.*;

public class CountryParticipantImplTest {

    public static CountryParticipantImpl country = new CountryParticipantImpl();

    @Before
    public void setUp() throws Exception {
        country.addParticipant(FIRST_PARTICIPANT);
    }

    @Test
    public void getName() {
        country.setName("Россия");
        assertEquals("Россия",country.getName());
    }

    @Test
    public void setName() {
        country.setName("Россия");
        assertEquals("Россия",country.getName());

        country.setName("Japan");
        assertEquals("Japan",country.getName());
    }

    @Test
    public void addParticipant() {
        country.getParticipants().clear();
        country.addParticipant(FIRST_PARTICIPANT);
        country.addParticipant(SECOND_PARTICIPANT);
        country.addParticipant(THREE_PARTICIPANT);
        assertEquals(3,country.getParticipants().size());
    }

    @Test
    public void getParticipants() {
        country.addParticipant(SECOND_PARTICIPANT);
        country.addParticipant(THREE_PARTICIPANT);

        List<Participant> list = country.getParticipants();

        assertEquals(list.get(0),country.getParticipants().get(0));
        assertEquals(list.get(1),country.getParticipants().get(1));
        assertEquals(list.get(2),country.getParticipants().get(2));
    }

    @Test
    public void setScore() {
        country.setScore(10);
        assertEquals(10,country.getScore());

        country.setScore(20);
        assertEquals(30,country.getScore());
    }

    @Test
    public void getScore() {
        assertEquals(30,country.getScore());
    }
}