package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static ru.edu.model.AthleteImplTest.FIRST_ATHLETE;
import static ru.edu.model.AthleteImplTest.SECOND_ATHLETE;
import static ru.edu.model.AthleteImplTest.THREE_ATHLETE;

public class ParticipantImplTest {

    public static ParticipantImpl FIRST_PARTICIPANT = new ParticipantImpl(1l, FIRST_ATHLETE);
    public static ParticipantImpl SECOND_PARTICIPANT = new ParticipantImpl(2l, SECOND_ATHLETE);
    public static ParticipantImpl THREE_PARTICIPANT = new ParticipantImpl(3l, THREE_ATHLETE);

    public static List<Participant> list;

    @Before
    public void setUp() throws Exception {
        list = new ArrayList<>();
        list.add(FIRST_PARTICIPANT);
        list.add(SECOND_PARTICIPANT);
        list.add(THREE_PARTICIPANT);
    }

    @Test
    public void getId() {
        assertEquals(FIRST_PARTICIPANT.getId(), list.get(0).getId());
        assertEquals(SECOND_PARTICIPANT.getId(), list.get(1).getId());
        assertEquals(THREE_PARTICIPANT.getId(), list.get(2).getId());
    }

    @Test
    public void getAthlete() {
        assertEquals(FIRST_PARTICIPANT.getAthlete(), list.get(0).getAthlete());
        assertEquals(SECOND_PARTICIPANT.getAthlete(), list.get(1).getAthlete());
        assertEquals(THREE_PARTICIPANT.getAthlete(), list.get(2).getAthlete());
    }

    @Test
    public void getScore() {
        assertEquals(FIRST_PARTICIPANT.getScore(), list.get(0).getScore());
        assertEquals(SECOND_PARTICIPANT.getScore(), list.get(1).getScore());
        assertEquals(THREE_PARTICIPANT.getScore(), list.get(2).getScore());
    }

    @Test
    public void incrementParticipantTest() {
        FIRST_PARTICIPANT.incrementScore(10);
        SECOND_PARTICIPANT.incrementScore(20);
        THREE_PARTICIPANT.incrementScore(30);

        assertEquals(10, FIRST_PARTICIPANT.getScore());
        assertEquals(20, SECOND_PARTICIPANT.getScore());
        assertEquals(30, THREE_PARTICIPANT.getScore());

        FIRST_PARTICIPANT.incrementScore(-10);
        SECOND_PARTICIPANT.incrementScore(-20);
        THREE_PARTICIPANT.incrementScore(-30);

        assertEquals(0, FIRST_PARTICIPANT.getScore());
        assertEquals(0, SECOND_PARTICIPANT.getScore());
        assertEquals(0, THREE_PARTICIPANT.getScore());
    }
}