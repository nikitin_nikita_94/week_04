package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static ru.edu.model.AthleteImplTest.*;
import static ru.edu.model.ParticipantImplTest.*;

public class CompetitionImplTest {

    CompetitionImpl competition = new CompetitionImpl();

    @Test
    public void register() {
        competition.register(FIRST_ATHLETE);
        competition.register(SECOND_ATHLETE);
        assertEquals(2, competition.getParticipantMap().size());

        competition.register(THREE_ATHLETE);

        assertEquals(3, competition.getParticipantMap().size());
        assertEquals(FIRST_PARTICIPANT, competition.getParticipantMap().get(1L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerDuplicateException() {
        competition.register(FIRST_ATHLETE);
        competition.register(FIRST_ATHLETE);
    }

    @Test
    public void updateScore() {
        competition.register(FIRST_ATHLETE);
        competition.register(SECOND_ATHLETE);

        competition.updateScore(1L, 10);
        competition.updateScore(2L, 20);

        assertEquals(10, competition.getParticipantMap().get(1L).getScore());
        assertEquals(20, competition.getParticipantMap().get(2L).getScore());

        competition.updateScore(1L, -5);
        competition.updateScore(2L, -5);

        assertEquals(5, competition.getParticipantMap().get(1L).getScore());
        assertEquals(15, competition.getParticipantMap().get(2L).getScore());
    }

    @Test
    public void updateScoreParticipant() {
        competition.register(FIRST_ATHLETE);
        competition.register(SECOND_ATHLETE);

        competition.updateScore(FIRST_PARTICIPANT, 10);
        competition.updateScore(SECOND_PARTICIPANT, 20);

        assertEquals(10, competition.getParticipantMap().get(1L).getScore());
        assertEquals(20, competition.getParticipantMap().get(2L).getScore());

        competition.updateScore(FIRST_PARTICIPANT, -5);
        competition.updateScore(SECOND_PARTICIPANT, -5);

        assertEquals(5, competition.getParticipantMap().get(1L).getScore());
        assertEquals(15, competition.getParticipantMap().get(2L).getScore());
    }

    @Test
    public void getResults() {
        String expected = competition.getResults().toString();
        System.out.println(expected);
        assertEquals(expected, competition.getResults().toString());

        competition.register(THREE_ATHLETE);
        competition.updateScore(FIRST_PARTICIPANT, 10);
        competition.updateScore(SECOND_PARTICIPANT, 20);
        competition.updateScore(THREE_PARTICIPANT, 30);

        expected = competition.getResults().toString();

        System.out.println(expected);
        assertEquals(expected, competition.getResults().toString());
    }

    @Test
    public void getParticipantsCountries() {
        competition.getParticipantMap().clear();
        competition.register(FIRST_ATHLETE);
        competition.register(SECOND_ATHLETE);
        competition.register(THREE_ATHLETE);
        competition.register(FORE_ATHLETE);
        competition.register(FIVE_ATHLETE);
        competition.register(SIX_ATHLETE);


        competition.updateScore(1L,10);
        competition.updateScore(2L,20);
        competition.updateScore(3L,30);
        competition.updateScore(4L,40);
        competition.updateScore(5L,50);
        competition.updateScore(6L,60);

        String expected = competition.getParticipantsCountries().toString();
        System.out.println(expected);

        assertEquals(expected,competition.getParticipantsCountries().toString());
    }
}