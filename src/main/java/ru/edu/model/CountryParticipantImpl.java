package ru.edu.model;

import java.util.ArrayList;
import java.util.List;

public class CountryParticipantImpl implements CountryParticipant {

    /**
     * Имя страны.
     */
    private String name;

    /**
     * Список участников.
     */
    private List<Participant> participants = new ArrayList<>();

    /**
     * Счет.
     */
    private long score;

    /**
     * Название страны.
     *
     * @return название
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Добовляем или обновляем ими страны.
     * @param name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Добовляет участников от стран в список.
     * @param value
     */
    public void addParticipant(final Participant value) {
        participants.add(value);
        setScore(value.getScore());
        if (name == null) {
            setName(value.getAthlete().getCountry());
        }
    }

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    @Override
    public List<Participant> getParticipants() {
        return participants;
    }

    /**
     * Счет страны.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    /**
     * Обновляем счет.
     * @param score
     */
    public void setScore(final long score) {
        this.score += score;
    }

    /**
     * Выводит имя страны и счет.
     * @return
     */
    @Override
    public String toString() {
        return "CountryParticipantImpl{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
