package ru.edu.model;

import java.util.List;

/**
 * Информация о стране-участнике
 */
public interface CountryParticipant {

    /**
     * Название страны.
     *
     * @return название
     */
    String getName();

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    List<Participant> getParticipants();

    /**
     * Счет страны.
     *
     * @return счет
     */
    long getScore();
}
