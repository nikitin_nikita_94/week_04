package ru.edu.model;

import java.util.Objects;

public class AthleteImpl implements Athlete {

    /**
     * Имя атлета.
     */
    private String firstName;

    /**
     * Фамилия атлета.
     */
    private String lastName;

    /**
     * Страна атлета.
     */
    private String country;

    /**
     * Конструктор.
     *
     * @param firstName
     * @param lastName
     * @param country
     */
    public AthleteImpl(final String firstName, final String lastName, final String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    /**
     * Метод для сравнения обьектов на равность между собой.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AthleteImpl athlete = (AthleteImpl) o;
        return firstName.equals(athlete.firstName)
                && lastName.equals(athlete.lastName)
                && country.equals(athlete.country);
    }

    /**
     * Метод возвращатет Хэшкод обьекта.
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country);
    }

    /**
     * Вывод полей обьекта Атлета.
     */
    @Override
    public String toString() {
        return "AthleteImpl{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
