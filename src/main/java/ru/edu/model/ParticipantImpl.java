package ru.edu.model;

import java.util.Objects;

public class ParticipantImpl implements Participant {

    /**
     * Регистрационный номер.
     */
    private Long id;

    /**
     * Обьект спортсмена.
     */
    private Athlete athlete;

    /**
     * Счет.
     */
    private long score;

    /**
     * Конструктор.
     *
     * @param id
     * @param athlete
     */
    public ParticipantImpl(final Long id, final Athlete athlete) {
        this.id = id;
        this.athlete = athlete;
        this.score = 0;
    }

    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Информация о спортсмене
     *
     * @return объект спортсмена
     */
    @Override
    public Athlete getAthlete() {
        return athlete;
    }

    /**
     * Счет участника.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    /**
     * Обновление счета участника.
     *
     * @param score
     */
    public void incrementScore(final long score) {
        this.score += score;
    }

    /**
     * Метод для сравнения обьектов на равность между собой.
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipantImpl that = (ParticipantImpl) o;
        return Objects.equals(athlete, that.athlete);
    }

    /**
     * Метод возвращатет Хэшкод обьекта.
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(athlete);
    }

    /**
     * Вывод всех полей класса.
     *
     * @return
     */
    @Override
    public String toString() {
        return "ParticipantImpl{" +
                "id=" + id +
                ", athlete=" + athlete +
                ", score=" + score +
                '}';
    }
}
