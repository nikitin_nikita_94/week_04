package ru.edu;

import ru.edu.model.*;

import java.util.*;
import java.util.stream.Collectors;

public class CompetitionImpl implements Competition {

    /**
     * Регистрационный номер участника.
     */
    private Long id = 1L;

    /**
     * Карта для хранения обьекта Participant по id.
     */
    private Map<Long, Participant> participantMap = new HashMap<>();

    /**
     * Получение карты.
     *
     * @return
     */
    public Map<Long, Participant> getParticipantMap() {
        return participantMap;
    }

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(final Athlete participant) {
        ParticipantImpl registerParticipant = new ParticipantImpl(id, participant);
        if (participantMap.containsValue(registerParticipant)) {
            throw new IllegalArgumentException("Duplicate Participant Object");
        }

        participantMap.put(id++, registerParticipant);
        return getClone(registerParticipant);
    }

    /**
     * Создает новый обьект с такими же полями, что у Обьекта в аргументах метода getCLone().
     *
     * @param registerParticipant
     * @return
     */
    private ParticipantImpl getClone(final Participant registerParticipant) {
        return new ParticipantImpl(registerParticipant.getId(),
                registerParticipant.getAthlete());
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(final long id, final long score) {
        ParticipantImpl participantUpdate = (ParticipantImpl) participantMap.get(id);

        if (participantUpdate != null) {
            participantUpdate.incrementScore(score);
            participantMap.put(id, participantUpdate);
        }
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(final Participant participant, final long score) {
        updateScore(participant.getId(), score);
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        List<Participant> list = participantMap.values()
                .stream()
                .sorted(resultsScoreParticipantComparator())
                .collect(Collectors.toList());

        return list;
    }

    /**
     * Метод для сравнения участников от большего счета к меньшему.
     *
     * @return
     */
    private Comparator<Participant> resultsScoreParticipantComparator() {
        Comparator<Participant> comparator = (o1, o2) -> {
            if (o1.getScore() == o2.getScore()) {
                return 0;
            }
            return o1.getScore() > o2.getScore() ? -1 : 1;
        };
        return comparator;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        Map<String, CountryParticipantImpl> mapParticipantsCountries = new HashMap<>();
        for (Map.Entry<Long, Participant> value : participantMap.entrySet()) {
            String countryName = value.getValue().getAthlete().getCountry();
            Participant athlete = value.getValue();

            if (!mapParticipantsCountries.containsKey(countryName)) {
                mapParticipantsCountries.put(countryName, new CountryParticipantImpl());
            }

            CountryParticipantImpl countryParticipant = mapParticipantsCountries.get(countryName);
            countryParticipant.addParticipant(athlete);
        }

        List<CountryParticipant> participantList = mapParticipantsCountries
                .values()
                .stream()
                .sorted((o1, o2) -> {
                    if (o1.getScore() == o2.getScore()) {
                        return 0;
                    }
                    return o1.getScore() > o2.getScore() ? -1 : 1;
                })
                .collect(Collectors.toList());

        return participantList;
    }
}
